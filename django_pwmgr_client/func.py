import requests
import os


def pwmgr_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "PWMGR_API_URL",
            default="https://pwmgr-api.example.com/",
        )

    return settings.PWMGR_API_URL


def pwmgr_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("PWMGR_API_USER", default="user")
        password = os.getenv("PWMGR_API_PASSWD", default="secret")
        return username, password

    username = settings.PWMGR_API_USER
    password = settings.PWMGR_API_PASSWD

    return username, password


def pwmgr_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{pwmgr_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def pwmgr_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{pwmgr_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def pwmgr_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{pwmgr_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def pwmgr_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{pwmgr_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def pwmgr_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{pwmgr_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def pwmgr_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{pwmgr_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def pwmgr_get_api_credentials_for_customer(mdat_id: int):
    username, password = pwmgr_default_credentials()

    if username != mdat_id:
        response = pwmgr_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def pwmgr_get_single_credentials(mdat_id: int, cred_id: str):
    username, password = pwmgr_get_api_credentials_for_customer(mdat_id)

    response = pwmgr_get_data(
        f"v1/credential/{cred_id}",
        username,
        password,
    )

    return response


def pwmgr_set_single_credentials(
    mdat_id: int, org_tag: str, folder: str, cred_data: dict, cred_id=None
):
    username, password = pwmgr_get_api_credentials_for_customer(mdat_id)

    cred_data["org_tag"] = org_tag
    cred_data["folder"] = folder

    if cred_id is not None:
        cred_data["id"] = cred_id

    response = pwmgr_post_data(
        "v1/credential",
        username,
        password,
        data=cred_data,
    )

    return response


def pwmgr_get_otp(mdat_id: int, cred_id: str):
    username, password = pwmgr_get_api_credentials_for_customer(mdat_id)

    response = pwmgr_get_data(
        f"v1/otp/{cred_id}",
        username,
        password,
    )

    return response
